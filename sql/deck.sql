CREATE TABLE deck (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    master CHAR(13) NOT NULL,
    client CHAR(13) NOT NULL,
    UNIQUE idx_master (master),
    UNIQUE idx_client (client)
) ENGINE=INNODB CHARSET=utf8;
