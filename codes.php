<?php
/**
 * Create a set of codes and return them to the client as a JSON object.
 */

require 'config.php';

$sql = 'INSERT deck SET '
    . 'master = :master, '
    . 'client = :client';
$parameters = array(
    'master' => uniqid(),
    'client' => uniqid(),
);

$db = new PDO('mysql:host=' . $config->dbHost . ';dbname=' . $config->dbName,
    $config->dbUser, $config->dbPass);
$sth = $db->prepare($sql);
$sth->execute($parameters);

header('Content-type: application/json');
echo json_encode($parameters);
