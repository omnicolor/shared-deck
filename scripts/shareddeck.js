/**
 * @fileoverview Javascript behavior for shared deck code generator.
 */

goog.require('goog.debug.ErrorHandler');
goog.require('goog.dom');
goog.require('goog.events');
goog.require('goog.events.EventHandler');
goog.require('goog.events.EventTarget');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');


/**
 * Shared deck code generator behavior.
 */
var shareddeck = {};


/**
 * Initialize the shared deck code generator behavior.
 */
shareddeck.init = function() {
    /** @type {Element} */
    var el = goog.dom.getElement('submit');

    goog.events.listen(el, goog.events.EventType.CLICK, shareddeck.submitClick);
};


/**
 * Handle the button click.
 * @param {goog.events.Event} e Event that fired the handler (button click).
 */
shareddeck.submitClick = function(e) {
    e.preventDefault();

    /** @type {!goog.net.XhrIo} */
    var xhr = new goog.net.XhrIo();
    goog.events.listen(xhr, goog.net.EventType.COMPLETE,
        shareddeck.codeResponse);
    xhr.send('/codes.php', 'PUT');
};


/**
 * Handle the XHR response.
 * @param {goog.events.Event} e Event that fired the callback (xhr).
 */
shareddeck.codeResponse = function(e) {
    /** @type {!Object} */
    var obj = e.target.getResponseJson();
    goog.dom.getElement('master').value = obj['master'];
    goog.dom.getElement('client').value = obj['client'];
};

goog.events.listen(window, goog.events.EventType.LOAD, shareddeck.init);
