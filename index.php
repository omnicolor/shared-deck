<!doctype html>
<html>
<head>
    <title>Shared Deck</title>
</head>

<body>
<h1>Shared Deck</h1>

<p>Click the button to create some codes for your shared deck.</p>

<form>
    <div>
        <label for="master">Master</label>
        <input disabled id="master" type="text">
        <label for="client">Client</label>
        <input disabled id="client" type="text">
    </div>
    <button id="submit">Generate codes</button>
</form>

<script src="shareddeck.js"></script>
</body></html>
