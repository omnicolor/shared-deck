SharedDeck
==========

Allows remote users to follow along with a web presentation automatically.

This was basically meant to be a replacement for Google Docs when they
took away the ability for remote viewers to follow along with a
presentation.

Released under the Apache 2.0 license.
